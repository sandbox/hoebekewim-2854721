<?php

/**
 * @file
 * Form alters for Default Menu Link.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function default_menu_link_form_node_form_alter(array &$form, FormStateInterface $form_state, $form_id) {

  if (method_exists($form_state->getFormObject(), 'getEntity')) {
    $node = $form_state->getFormObject()->getEntity();
    // Only load this logic on a node.
    if ($node !== NULL && $node instanceof Node) {

      // Retrieve the node type entity
      $type = NodeType::load($node->getType());

      // Check if the settings has been enabled
      $enabled = $type->getThirdPartySetting('default_menu_link', 'default_menu_link_enabled', FALSE);

      // Do not apply to already enabled checkboxes and to existing nodes
      if ($node->get('nid')->value === null && isset($form['menu']) && $form['menu']['enabled']['#default_value'] !== 1 && $enabled === TRUE) {
        $form['menu']['#open'] = $form['menu']['enabled']['#default_value'] = $enabled;

        if (empty($form['menu']['link']['title']['#default_value']) && $node->getTitle()) {
          $form['menu']['link']['title']['#default_value'] = $node->getTitle();
        }
      }
    }
  }
}
