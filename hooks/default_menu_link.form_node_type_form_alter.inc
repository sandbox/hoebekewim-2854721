<?php

/**
 * @file
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeTypeInterface;

/**
 * Implements hook_form_FORM_ID_alter.
 *
 * @param array $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 */
function default_menu_link_form_node_type_form_alter(array &$form, FormStateInterface $form_state) {

  // Retrieve the entity to be able to store data.
  $type = $form_state->getFormObject()->getEntity();

  // A new tab needs to be created to avoid issues with the default ajax calls on the menu settings tab.
  if (!isset($form['menu_options'])) {
    $form['menu_options'] = [
      '#type' => 'details',
      '#title' => t('Menu options'),
      '#group' => 'additional_settings',
    ];
  }

  // Add a checkbox to the existing menu items.
  $form['menu_options']['default_menu_link_enabled'] = [
    '#type' => 'checkbox',
    '#title' => t('Default the "Provide a menu link" item to checked.'),
    '#description' => t('Enable the "Provide a menu link" checkbox by default.'),
    '#default_value' => $type->getThirdPartySetting('default_menu_link', 'default_menu_link_enabled', FALSE),
  ];

  // Add a submit handler to store the custom checkbox.
  $form['#entity_builders'][] = 'default_menu_link_form_node_type_form_builder';
}

/**
 * Entity builder for the node type form with scheduler options.
 *
 * @param $entity_type
 * @param \Drupal\node\NodeTypeInterface $type
 * @param $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 */
function default_menu_link_form_node_type_form_builder($entity_type, NodeTypeInterface $type, &$form, FormStateInterface $form_state) {
  $type->setThirdPartySetting(
    'default_menu_link',
    'default_menu_link_enabled',
    $form_state->getValue('default_menu_link_enabled')
  );
}
